import React from 'react';
import { Container, Content, Header, Body, Left, Right, Button, Text, Icon, FooterTab, Footer, Card, CardItem,Thumbnail, Input, Item} from 'native-base';
import { Image, ListView , TouchableOpacity, Platform } from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import Actions from '../../state/card/action'


@connect((data, props) => Home.getDataProps(data, props))
export default class Home extends React.Component {

	constructor(props) {
		super(props);
		this.state = {
		  index:0,
		  searchText:"",
		  isSearching:false,
		}
		this.props.dispatch(Actions.GetALlCards());
	}

	static getDataProps(state, props) {
	 
		return {
      cards:state.cards.ALL.data ? state.cards.ALL.data : [], 
		}
	}

	render() {
		return (
			<Container>
			  {this.state.isSearching ?
				  <Header searchBar rounded  style={{backgroundColor:"#eee"}}>
              <Item>
               <Button transparent onPress={()=>this.setState({isSearching:false, searchText:""})}>
                  <Icon name='arrow-back' style={{color:"black",  marginTop: Platform.OS === 'ios' ? -12 :0}} />
                </Button>
                <Input placeholder="Search" onChangeText={(text)=>{this.setState({searchText:text})}}/>
                <Icon name="ios-search" />
              </Item>
          </Header>
				:
  				<Header style={{backgroundColor:"#eee"}}>
  					<Left/>
  						
  					<Body>
  						<Text>
  					  	{"Card Demo app"}
  						</Text>
  					</Body>
  					<Right>
  					  <Button transparent onPress={()=>this.setState({isSearching:true})}>
                	<Icon name="ios-search" style={{color:'black'}}/>
              </Button>
  					</Right>
  				</Header>
			  }
				<Content>
          {
					  this.props.cards.filter((data)=>(data.title.toLowerCase().includes(this.state.searchText.toLowerCase()))).map((data)=>{
					    const now = moment();
					    const date = moment(data.createdAt);
					    return (
					      <TouchableOpacity  onPress={()=>{this.props.navigation.navigate("Item",{id:data.id})}}>
  					      <Card>
                    <CardItem>
                      <Left>
                        <Thumbnail source={{uri:data.image}} />
                        <Body>
                          <Text>{data.title}</Text>
                          <Text note>{data.content.substr(0,45)+"......"}</Text>
                        </Body>
                      </Left>
                    </CardItem>
                    <CardItem cardBody>
                      <Image source={{uri: data.image}} style={{height: 200, width: null, flex: 1}}/>
                    </CardItem>
                    <CardItem>
                      <Left>
                        <Button transparent>
                          <Icon active name="thumbs-up" />
                          <Text>12 Likes</Text>
                        </Button>
                      </Left>
                      <Body>
                        <Button transparent>
                          <Icon active name="chatbubbles" />
                          <Text>4 Comments</Text>
                        </Button>
                      </Body>
                      <Right>
                        <Text>{now.diff(date,'month') +" moths ago"}</Text>
                      </Right>
                    </CardItem>
                  </Card>
                </TouchableOpacity>
              )
					  })
					}
				
				</Content>
				
			</Container>
		);
	}
}
/*	 <ListView
            data={this.props.cards.filter((data, index)=>(index >= this.state.index))}
            renderRow={({data})=>{
              return(
                <Card>
                  <CardItem>
                    <Left>
                      <Thumbnail source={{uri:data.image}} />
                      <Body>
                        <Text>{data.title}</Text>
                        <Text note>{data.image}</Text>
                      </Body>
                    </Left>
                  </CardItem>
                  <CardItem cardBody>
                    <Image source={{uri: data.image}} style={{height: 200, width: null, flex: 1}}/>
                  </CardItem>
                  <CardItem>
                    <Left>
                      <Button transparent>
                        <Icon active name="thumbs-up" />
                        <Text>12 Likes</Text>
                      </Button>
                    </Left>
                    <Body>
                      <Button transparent>
                        <Icon active name="chatbubbles" />
                        <Text>4 Comments</Text>
                      </Button>
                    </Body>
                    <Right>
                      <Text>11h ago</Text>
                    </Right>
                  </CardItem>
                </Card>
              )
            }}
           
            onLoadMoreAsync={()=>this.setState({index:++this.state.index})}
          />*/
