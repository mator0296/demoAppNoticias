import React from 'react';
import { Container, Content, Header, Body, Left, Right, Button, Text, Icon, FooterTab, Footer, Card, CardItem,Thumbnail, Input} from 'native-base';
import { Image, View } from 'react-native';
import { connect } from 'react-redux';
import Actions from '../../state/card/action'
import ParallaxScrollView from 'react-native-parallax-scroll-view';
import Moment from 'moment';

@connect((data, props) => Item.getDataProps(data, props))
export default class Item extends React.Component {

	constructor(props) {
		super(props);
	
		this.state = {
		  id: this.props.navigation.getParam('id'),
		}
		this.props.dispatch(Actions.getCard(this.state.id));
	}

	static getDataProps(state, props) {
		return {
      item:state.cards.Value ? state.cards.Value : {}, 
		}
	}
  /* <View style={{ height: 300, flex: 1, alignItems: 'center', justifyContent: 'center' }}>
    <Text style={{color:'black', fontSize:25, textAlign:'center'}}>{this.props.item.title}</Text>
      </View>
  */
	render() {
		return (
			<Container>
			  <Header style={{backgroundColor:"#eee"}}>
					<Left>
					  <Button transparent onPress={()=>this.props.navigation.goBack()}>
              <Icon name='arrow-back' style={{color:"black"}} />
            </Button>
					</Left>
					<Body>
						<Text>
						  {"Card Demo app"}
						</Text>
					</Body>
					<Right/>
				</Header>
				<Content>
    			<ParallaxScrollView
            backgroundColor="black"
            contentBackgroundColor="black"
            parallaxHeaderHeight={250}
            renderForeground={() => (
              <Image source={{uri: this.props.item.image}} style={{zIndex:1,height: "100%", width: "100%"}}/>
            )}>
              <View style={{ height: 700, underlineColor:"black",borderRadius:15,borderWidth:1, borderColor:'black', backgroundColor:"white"}}>
                <Text style={{padding:15, fontSize:25, textAlign:'center'}}>{this.props.item.title}</Text>
                <Text style={{padding:15, fontSize:20, textAlign:'justify'}}>{this.props.item.content}</Text>
                <Text style={{padding:15, fontSize:20, textAlign:'justify'}}>{"published on " +  Moment(this.props.item.createdAt).format("dddd", "MMMM DO YYYY, h:mm:ss a")}</Text>
              </View>
          </ParallaxScrollView>
				</Content>
			</Container>
		);
	}
}