const styles = {

	container: {
		width:"100%",
		paddingRight:"10%",
		paddingLeft:"10%",
		flex:1,
		alignItems:'center',
		backgroundColor:'white'
	},

	itemContainer:{
		marginTop:'5%',
		
	},

	itemHeader:{
		fontSize:24,
		fontWeight:"bold",
		textAlign:'center',
	},
	inputData:{
		width:"100%"
	},
	footer:{
		justifyContent:'center',
		backgroundColor:'white'
	}
}

export default styles;