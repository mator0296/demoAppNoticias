import * as ActionsTypes from './types';
import Base from '../base/action';
import CardService from '../../services/card';
export default class Actions extends Base {
	
	static GetALlCards = () => (dispatch, state) => {
    
		dispatch(Actions.fetchDataRequest());
		return CardService.getCards().then((cards)=>{
			dispatch({
				type: ActionsTypes.GET_CARD,
				payload:{cards}
			})

		})
		.then(() => dispatch(Actions.fetchDataFinished()))
        .catch((err) => {
            dispatch(Actions.fetchDataError(err))  
        })

	}

	static getCard = (id) => (dispatch, state) =>{
		dispatch(Actions.fetchDataRequest());
		return CardService.getCardId(id).then((card)=>{
			dispatch( {
				type: ActionsTypes.GET_CARDS_ID,
				payload:{card}
			})

		})
		.then(() => dispatch(Actions.fetchDataFinished()))
        .catch((err) => {
            dispatch(Actions.fetchDataError(err))  
        })
	}

	
}