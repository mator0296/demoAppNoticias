import * as Action from './types';
import Card from './record';

class CardReducer {
	static reducer(card = new Card(), action){
		if (CardReducer[action.type]) {
			return CardReducer[action.type](card, action);
		}
		return card;
		
	}

	static [Action.GET_CARD](state, action){
		const cards = action.payload.cards
		return state.set('ALL',cards);
	}

	static [Action.GET_CARDS_ID](state, action){
		const card = action.payload.card
		return state.set('Value',card);
	}

}
export default CardReducer.reducer;

