import CardReducer from './card/reducer';
import BaseReducer from './base/reducer';
import thunk from 'redux-thunk';
import { applyMiddleware, combineReducers, createStore } from 'redux';

export default createStore(
	combineReducers({
		cards:CardReducer,
		base: BaseReducer,
	}),
	applyMiddleware(thunk)
);