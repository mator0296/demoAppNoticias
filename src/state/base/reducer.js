import Action from './action';
import Base from './record';

class BaseReducer {
	static reducer(base = new Base(), action){
		if (BaseReducer[action.type]) {
			return BaseReducer[action.type](base, action);
		}
		return base;
		
	}

	static [Action.DATA_ERROR](state, action){
		const error = action.payload.error
		return state.merge({isFeching:false, error});
	}

	static [Action.DATA_FECTH](state, action){
		return state.set("isFeching" ,true);
	}

	static [Action.DATA_RECEIVED](state, action){
		return state.set("isFeching" ,false);
	}
}

export default BaseReducer.reducer;