import * as ActionTypes from './types';

export default class Actions {
    
    static fetchDataRequest() {
        return {
            type: ActionTypes.DATA_FECTH,
            payload:{isFetching:true},
        }
    }
    static fetchDataFinished() {
        return {
            type: ActionTypes.DATA_RECEIVED,
            payload:{isFetching:false},
        }
    }
    static fetchDataError(error) {
        return {
            type: ActionTypes.DATA_ERROR,
            payload:{isFetching:false,error},
        }
    }
}
