import home from '../screens/home';
import Item from '../screens/home/item';
import { createStackNavigator } from 'react-navigation';

const routes = createStackNavigator({
		Home:{
			screen:home
		},
		Item:{
			screen:Item
		},
	},
	{
		headerMode:'none',
		navigationOptions:{
			headerVisible:false
		}
	}
)

export default routes;
