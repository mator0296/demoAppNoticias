'use strict';

import React from 'react';
 
import {
  View,
  Text, 
  StyleSheet
} from 'react-native';
import { Provider as ReduxProvider, connect } from 'react-redux';
import store from './state/store'

import HomeNav from './navigation/homeNav';


export default class AppWrapper extends React.Component {

      render() {
        return (
          <ReduxProvider store={store}>
            <AppState/>
          </ReduxProvider>
        );
      }
}


@connect((state,props)=>AppState.getData(state, props))
class AppState extends React.Component {

  static getData(state, props){

    return {
      cards:state.cards,
      
    }
  }
  renderNav(){
    
      return <HomeNav/>
    
  }

  render() {
    return (
      <View style={{flex:1, backgroundColor:'white'}}>
      	
      	{this.renderNav()}

      </View>
    );
  }
}
